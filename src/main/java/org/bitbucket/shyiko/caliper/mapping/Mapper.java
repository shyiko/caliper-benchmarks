package org.bitbucket.shyiko.caliper.mapping;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;
import org.bitbucket.shyiko.caliper.mapping.model.*;
import org.dozer.DozerBeanMapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import java.util.Arrays;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public interface Mapper {
  
  <T> T map(Object source, Class<T> targetClass);
  
  public class Builder {

    private int usage;
    private Mapper mapper;
    
    public Builder useDozerViaXML() {
      usage++;
      mapper = new Mapper() {

        private final DozerBeanMapper delegatee;

        {
          delegatee = new DozerBeanMapper(Arrays.asList("dozer-mappings.xml"));
        }

        @Override
        public Object map(Object source, Class targetClass) {
          return delegatee.map(source, targetClass);
        }
      };
      return this;
    }

    public Builder useModelMapper() {
      usage++;
      mapper = new Mapper() {

        private final ModelMapper delegatee;

        {
          delegatee = new ModelMapper();
          delegatee.addMappings(new PropertyMap<OrderDTO, Order>() {

            @Override
            protected void configure() {
              map().getBillingAddress().setCity(source.getBillingCity());
              map().getBillingAddress().setStreet(source.getBillingStreet());
            }
          });
        }

        @Override
        public Object map(Object source, Class targetClass) {
          return delegatee.map(source, targetClass);
        }
      };
      return this;
    }

    public Builder useOrika() {
      usage++;
      mapper = new Mapper() {

        private final MapperFacade delegatee;

        {
          ma.glasnost.orika.MapperFactory factory = new DefaultMapperFactory.Builder().build();
          factory.registerClassMap(ClassMapBuilder.map(Order.class, OrderDTO.class).
                  field("customer.name.firstName", "customerFirstName").
                  field("customer.name.lastName", "customerLastName").
                  field("billingAddress.street", "billingStreet").
                  field("billingAddress.city", "billingCity").
                  toClassMap());
          factory.build();
          delegatee = factory.getMapperFacade();
        }

        @Override
        public Object map(Object source, Class targetClass) {
          return delegatee.map(source, targetClass);
        }
      };
      return this;
    }
    
    public Mapper build() {
      if (usage == 0) {
        mapper = new Mapper() {

          @Override
          public <T> T map(Object source, Class<T> targetClass) {
            if (source instanceof Order) {
              Order order = (Order) source;
              Name name = order.getCustomer().getName();
              Address billingAddress = order.getBillingAddress();
              return (T) new OrderDTO(name.getFirstName(), name.getLastName(), billingAddress.getStreet(), billingAddress.getCity());
            } else {
              OrderDTO orderDTO = (OrderDTO) source;
              return (T) new Order(
                      new Customer(new Name(orderDTO.getCustomerFirstName(), orderDTO.getCustomerLastName())),
                      new Address(orderDTO.getBillingStreet(), orderDTO.getBillingCity())
              );
            }
          }
        };
      }
      if (usage > 1) {
        throw new IllegalStateException("Illegal usage of Mapper.Builder");
      }
      return mapper;
    }
  }
}
