package org.bitbucket.shyiko.caliper.mapping.model;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public class Order {

  private Customer customer;
  private Address billingAddress;

  public Order() {
  }

  public Order(Customer customer, Address billingAddress) {
    this.customer = customer;
    this.billingAddress = billingAddress;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Address getBillingAddress() {
    return billingAddress;
  }

  public void setBillingAddress(Address billingAddress) {
    this.billingAddress = billingAddress;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Order order = (Order) o;

    if (billingAddress != null ? !billingAddress.equals(order.billingAddress) : order.billingAddress != null)
      return false;
    if (customer != null ? !customer.equals(order.customer) : order.customer != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = customer != null ? customer.hashCode() : 0;
    result = 31 * result + (billingAddress != null ? billingAddress.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Order{" +
            "customer=" + customer +
            ", billingAddress=" + billingAddress +
            '}';
  }
}
