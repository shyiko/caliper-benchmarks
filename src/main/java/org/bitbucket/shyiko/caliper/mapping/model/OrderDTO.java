package org.bitbucket.shyiko.caliper.mapping.model;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public class OrderDTO {

  private String customerFirstName;
  private String customerLastName;
  private String billingStreet;
  private String billingCity;

  public OrderDTO() {
  }

  public OrderDTO(String customerFirstName, String customerLastName, String billingStreet, String billingCity) {
    this.customerFirstName = customerFirstName;
    this.customerLastName = customerLastName;
    this.billingStreet = billingStreet;
    this.billingCity = billingCity;
  }

  public String getCustomerFirstName() {
    return customerFirstName;
  }

  public void setCustomerFirstName(String customerFirstName) {
    this.customerFirstName = customerFirstName;
  }

  public String getCustomerLastName() {
    return customerLastName;
  }

  public void setCustomerLastName(String customerLastName) {
    this.customerLastName = customerLastName;
  }

  public String getBillingStreet() {
    return billingStreet;
  }

  public void setBillingStreet(String billingStreet) {
    this.billingStreet = billingStreet;
  }

  public String getBillingCity() {
    return billingCity;
  }

  public void setBillingCity(String billingCity) {
    this.billingCity = billingCity;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    OrderDTO orderDTO = (OrderDTO) o;

    if (billingCity != null ? !billingCity.equals(orderDTO.billingCity) : orderDTO.billingCity != null) return false;
    if (billingStreet != null ? !billingStreet.equals(orderDTO.billingStreet) : orderDTO.billingStreet != null)
      return false;
    if (customerFirstName != null ? !customerFirstName.equals(orderDTO.customerFirstName) : orderDTO.customerFirstName != null)
      return false;
    if (customerLastName != null ? !customerLastName.equals(orderDTO.customerLastName) : orderDTO.customerLastName != null)
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = customerFirstName != null ? customerFirstName.hashCode() : 0;
    result = 31 * result + (customerLastName != null ? customerLastName.hashCode() : 0);
    result = 31 * result + (billingStreet != null ? billingStreet.hashCode() : 0);
    result = 31 * result + (billingCity != null ? billingCity.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "OrderDTO{" +
            "customerFirstName='" + customerFirstName + '\'' +
            ", customerLastName='" + customerLastName + '\'' +
            ", billingStreet='" + billingStreet + '\'' +
            ", billingCity='" + billingCity + '\'' +
            '}';
  }
}
