package org.bitbucket.shyiko.caliper.mapping;

import com.google.caliper.Param;
import com.google.caliper.Runner;
import com.google.caliper.SimpleBenchmark;
import org.bitbucket.shyiko.caliper.mapping.model.*;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public class MappingBenchmark extends SimpleBenchmark {

  public enum MappingLibrary {

    HANDCRAFT {
      @Override
      public Mapper getMapper() {
        return new Mapper.Builder().build();
      }
    },
    ORIKA {
      @Override
      public Mapper getMapper() {
        return new Mapper.Builder().useOrika().build();
      }
    },
    MODELMAPPER {
      @Override
      public Mapper getMapper() {
        return new Mapper.Builder().useModelMapper().build();
      }
    },
    DOZER {
      @Override
      public Mapper getMapper() {
        return new Mapper.Builder().useDozerViaXML().build();
      }
    };

    public abstract Mapper getMapper();
  }

  @Param({"1", "100", "10000"})
  private int length;
  @Param
  private MappingLibrary mappingLibrary;

  private Order entity;
  private OrderDTO dto;
  private Mapper mapper;

  @Override
  public void setUp() throws Exception {
    entity = new Order(
            new Customer(new Name("first name", "last name")),
            new Address("billing street", "billing city")
    );
    dto = new OrderDTO("first name", "last name", "billing street", "billing city");
    mapper = mappingLibrary.getMapper();
  }

  public void timeMappingFromDTOToEntity(int reps) {
    for (int i = 0; i < reps; i++) {
      for (int j = 0; j < length; j++) {
        mapper.map(entity, OrderDTO.class);
      }
    }
  }

  public void timeMappingFromEntityToDTO(int reps) {
    for (int i = 0; i < reps; i++) {
      for (int j = 0; j < length; j++) {
        mapper.map(dto, Order.class);
      }
    }
  }

  public static void main(String[] args) throws Exception {
    Runner.main(MappingBenchmark.class, args);
  }
}
