package org.bitbucket.shyiko.caliper.mapping;

import org.bitbucket.shyiko.caliper.mapping.model.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static junit.framework.Assert.assertEquals;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public abstract class AbstractMappingTest {

  private Mapper mapper;
  private Order entity;
  private OrderDTO dto;

  protected AbstractMappingTest(Mapper mapper) {
    this.mapper = mapper;
  }

  @BeforeClass
  public void setUp() {
    entity = new Order(
            new Customer(new Name("first name", "last name")),
            new Address("billing street", "billing city")
    );
    dto = new OrderDTO("first name", "last name", "billing street", "billing city");
  }

  @Test
  public void testMappingFromDTOToEntity() {
    Order entity = mapper.map(dto, Order.class);
    assertEquals(this.entity, entity);
  }
  
  @Test
  public void testMappingFromEntityToDTO() {
    OrderDTO dto = mapper.map(entity, OrderDTO.class);
    assertEquals(this.dto, dto);
  }
}
