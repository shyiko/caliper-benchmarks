package org.bitbucket.shyiko.caliper.mapping;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public class ModelMapperTest extends AbstractMappingTest {

  public ModelMapperTest() {
    super(new Mapper.Builder().useModelMapper().build());
  }
}
