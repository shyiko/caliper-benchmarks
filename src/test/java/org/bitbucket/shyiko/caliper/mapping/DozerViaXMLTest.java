package org.bitbucket.shyiko.caliper.mapping;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public class DozerViaXMLTest extends AbstractMappingTest {

  public DozerViaXMLTest() {
    super(new Mapper.Builder().useDozerViaXML().build());
  }
}
