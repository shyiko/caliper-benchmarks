package org.bitbucket.shyiko.caliper.mapping;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public class OrikaTest extends AbstractMappingTest {

  public OrikaTest() {
    super(new Mapper.Builder().useOrika().build());
  }
}
