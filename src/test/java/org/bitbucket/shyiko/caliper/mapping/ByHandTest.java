package org.bitbucket.shyiko.caliper.mapping;

/**
 * @author <a href="mailto:stanley.shyiko@gmail.com">shyiko</a>
 */
public class ByHandTest extends AbstractMappingTest {

  public ByHandTest() {
    super(new Mapper.Builder().build());
  }
}
